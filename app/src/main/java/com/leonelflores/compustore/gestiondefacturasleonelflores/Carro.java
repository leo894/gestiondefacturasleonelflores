package com.leonelflores.compustore.gestiondefacturasleonelflores;


//es una clase publica
//"PUBLIC" es un modificador de acceso
public class Carro {
    //Propiedades

     private String color;
    private String motor,peso;
    private Integer anoFabricacion,NumeroLlantas;


    //Constructor de la clase
    //Tiene el mismo nombre de la clase
    //tiene el modificador de acceso public
    //no tiene tipo de datos
    public  Carro(){
        this.anoFabricacion=2014;

    }

    public String getColor() {
        return color;
    }

    // VOID es un procedimiento
    //procedimiento no devuelve un resultado a quien la llama.
    //el metodo setColor solicita que envien un parametro de tipo String
    public void setColor(String color) {

        //alcanze de variables
        //this.color es a nivel de clase
        //motor es a nivel de metodo
        this.color = color;
    }

    //es una función
    //se caracteriza porque devuelve un resultado a quien la llama
    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public Integer getAnoFabricacion() {
        return anoFabricacion;
    }

    public void setAnoFabricacion(Integer anoFabricacion) {
        this.anoFabricacion = anoFabricacion;
    }

    public Integer getNumeroLlantas() {
        return NumeroLlantas;
    }

    public void setNumeroLlantas(Integer numeroLlantas) {
        NumeroLlantas = numeroLlantas;
    }
//Metodos
    //sobrecarga
    public  void Encender(){
        //El carro se encendera.
    }

    public  void Encender (String codigo){

    }


}

