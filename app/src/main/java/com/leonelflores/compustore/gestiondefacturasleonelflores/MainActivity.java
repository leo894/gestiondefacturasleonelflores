package com.leonelflores.compustore.gestiondefacturasleonelflores;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

//Esto es una clase

public class MainActivity extends AppCompatActivity {

    //propiedades
    //nombre de la clase - nombre del objeto = new nombre de la clase-contructor;
    Carro carro = new Carro();

    //metodos
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.e("4C","OnCreate");

        Hibrido hibrido = new Hibrido();
        hibrido.getAnoFabricacion();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("4C","OnStar");
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onPause() {
        super.onPause();
        Log.e("4C","OnPause");

        if( isDestroyed()){
            Log.e("4C", "Destruida");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("4C","OnResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("4C","OnStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("4C", "OnDestroy");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("4C","OnRestard");
    }
}
